
package com.xyz.mokito.controller;

import com.xyz.mokito.entity.Employee;
import com.xyz.mokito.service.EmployeeService;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@WebMvcTest
public class EmployeeControllerTestCase {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    EmployeeService employeeService;

    @Test
    public void testFindOne() throws Exception {

        Mockito.when(employeeService.findById(1L)).thenReturn(new Employee(1L, "A1", "B1"));

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/employees/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("B1"));
        Mockito.verify(employeeService).findById(1L);
    }

    @Test
    public void testFindAll() throws Exception {
        Mockito.when(employeeService.findAllEmployee()).thenReturn(Collections.EMPTY_LIST);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/employees").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)).andReturn();
        Assert.assertEquals(200, mvcResult.getResponse().getStatus());
        Mockito.verify(employeeService).findAllEmployee();
    }

    @Test
    public void testCreateEmployee() throws Exception {
        Mockito.when(employeeService.createEmployee(new Employee(45L, "A", "B")))
                .thenReturn(new Employee(45L, "A", "B"));
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", 45L);
        map.put("name", "A");
        map.put("deptName", "B");
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONObject.toJSONString(map))
        ).andReturn();
        Assert.assertEquals(200, mvcResult.getResponse().getStatus());
    }
}
