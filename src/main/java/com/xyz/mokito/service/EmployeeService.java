
package com.xyz.mokito.service;

import com.xyz.mokito.dao.EmployeeRepository;
import com.xyz.mokito.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @PostConstruct
    public void populateBootstrapData() {
        employeeRepository.populateBootstrapData();
    }

    public List<Employee> findAllEmployee() {
        return employeeRepository.findAllEmployee();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepository.createEmployee(employee);
    }
}

