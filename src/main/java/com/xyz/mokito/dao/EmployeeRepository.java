package com.xyz.mokito.dao;

import com.xyz.mokito.entity.Employee;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    Map<Long, Employee> employeesRepo = null;

    @PostConstruct
    public void populateBootstrapData() {
        employeesRepo = new HashMap<>();
        for (long i = 0; i <= 20; ++i) {
            employeesRepo.put(i, new Employee(i, "MyName" + i, "MyDept" + i));
        }
    }


    public List<Employee> findAllEmployee() {
        return employeesRepo.values().parallelStream().collect(Collectors.toList());
    }

    public Employee findById(Long id) {
        return employeesRepo.get(id);
    }


    public Employee createEmployee(Employee employee) {
        employeesRepo.put(employee.getId(), employee);
        return employee;
    }
}
