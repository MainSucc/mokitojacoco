package com.xyz.mokito.service;

import com.xyz.mokito.dao.EmployeeRepository;
import com.xyz.mokito.entity.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    //mock对象
    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    public void findAllEmployee() {
        //指定mock对象的行为
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(99L, "zhang3", "dev"));
        Mockito.when(employeeRepository.findAllEmployee()).thenReturn(employees);
        //调用mock对象
        List<Employee> result = employeeService.findAllEmployee();
        //验证方法结果
        Assert.assertEquals(1, result.size());
        //验证方法被调用
        Mockito.verify(employeeRepository).findAllEmployee();
    }
}